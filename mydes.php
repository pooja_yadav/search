<html>
<body>
<?php
//permutation tables	
$permute_table_64 = array (
   58, 50, 42, 34, 26, 18, 10,  2,
   60, 52, 44, 36, 28, 20, 12,  4,
   62, 54, 46, 38, 30, 22, 14,  6,
   0, 56, 48, 40, 32, 24, 16,  8,
   57, 49, 41, 33, 25, 17,  9,  1,
   59, 51, 43, 35, 27, 19, 11,  3,
   61, 53, 45, 37, 29, 21, 13,  5,
   63, 55, 47, 39, 31, 23, 15,  7
);	
	
$permute_table_56 = array (
   57, 49, 41, 33, 25, 17,  9,  1,
   58,50, 42, 34, 26, 18, 10,  2,
	59, 51,43, 35, 27, 19, 11,  3,
	60, 52, 44,36, 63, 55, 47, 39, 
	31, 23, 15,  7,  62, 54, 46, 38,
	30, 22,14,  6, 61, 53, 45, 37,
	29, 21, 13,  5, 28, 20, 12,  4
  );
$permute_table_48 = array (
   14, 17, 11, 24,  1,  5,  3, 28,
   15,  6, 21, 10,23, 19, 12,  4,
    26,  8, 16,  7, 27, 20, 13,  2,
   41, 52, 31, 37, 47, 55, 30, 40,
	51, 45, 33, 48, 44, 49, 39, 0, 
	34, 53, 46, 42, 50, 36, 29, 32
);
$permute_table_32 = array (
 
   16,  7, 20, 21, 29, 12, 28,25,
   17,  1, 15, 23, 26,  5, 18, 31,
   10, 2,  8, 24, 14, 0, 27, 3,
	9, 19, 13, 30,  6, 22, 11,  4
);

$s_box_substitution = 
array(
array(
array(0,0,13,6,2,12,6,6,2,5,8,8,7,7,11,4),
array(2,14,9,5,5,9,9,11,10,0,0,15,11,10,4,11),
array(10,2,1,13,15,8,4,1,13,13,9,5,5,5,10,7),
array(4,3,13,9,12,6,5,7,6,6,6,1,0,11,13,11)
),
array(
array(14,15,9,13,7,14,15,4,11,8,10,0,14,4,8,2),
array(8,5,11,4,12,1,12,3,7,2,4,8,14,2,3,12),
array(1,13,10,9,11,9,14,7,2,8,8,0,13,0,2,5),
array(6,14,10,3,15,6,6,7,9,10,15,7,13,2,4,15)
),
array(
array(0,14,8,12,7,6,3,10,15,11,10,12,12,13,1,3),
array(11,11,6,11,2,12,2,11,7,1,2,5,4,7,4,5),
array(5,12,1,13,2,4,8,1,0,2,14,13,0,15,1,11),
array(11,7,7,13,4,9,8,12,11,11,1,0,3,5,5,8)
),
array(
array(1,6,6,4,11,14,6,12,1,4,10,1,4,11,13,15),
array(3,4,13,8,14,6,4,10,2,5,10,5,11,0,14,12),
array(6,5,1,2,3,7,14,5,12,8,6,0,4,4,0,7),
array(9,14,15,7,4,4,2,6,9,13,12,4,13,10,1,3)
),
array(
array(15,2,5,3,10,4,8,6,13,15,7,1,3,8,9,12),
array(6,8,4,11,12,7,1,6,4,14,11,1,8,12,4,8,),
array(15,10,11,9,14,4,0,12,4,7,13,8,15,6,5,5,),
array(15,9,1,12,0,2,2,4,1,13,5,9,10,10,2,9,),
),
array(
array(5,14,2,4,3,2,0,7,10,13,0,9,4,5,15,4,),
array(15,0,0,15,3,3,4,5,1,10,15,11,5,1,4,10),
array(15,7,15,3,10,15,10,4,13,10,14,2,0,14,6,15),
array(15,6,14,3,10,3,8,11,14,7,6,4,9,11,14,9)
),
array(
array(3,13,12,14,13,7,2,10,2,1,12,2,0,3,1,0),
array(10,0,4,4,4,13,15,2,5,6,6,14,2,5,7,6),
array(3,4,4,1,11,7,12,14,9,9,0,9,12,2,10,6),
array(2,14,11,7,11,11,9,0,2,0,15,4,6,7,11,10)
),
array(
array(11,15,11,7,7,7,5,0,0,6,10,13,8,4,3,11),
array(3,15,2,15,10,12,15,12,13,15,1,4,6,12,14,2),
array(12,10,10,3,1,0,4,2,6,14,0,15,3,4,11,6),
array(3,13,5,13,10,5,9,7,4,11,11,11,7,10,13,4),
)
);
for($i=0;$i<8;$i++)
	{
		for($j=0;$j<4;$j++)
		{
		for($k=0;$k<16;$k++)
		{   
			$rndm=$s_box_substitution[$i][$j][$k];
			$bitbinary = array();
			for ( $l= 3; $l >= 0; $l--)
           {   $x= ($rndm&(1 << $l))? 1 :0;  array_push($bitbinary, $x); }
            
	         $name=implode("",$bitbinary);
			$s_box_substitution[$i][$j][$k]=$name;
			$name=NULL;unset($bitbinary);
	      	 }
 }  }		
// show s boxes
	for($i=0;$i<8;$i++)
	{   for($j=0;$j<4;$j++)
	{    for($k=0;$k<15;$k++)
			{  echo $s_box_substitution[$i][$j][$k]."  " ; }
	           echo "<br>";
	}
		   echo "<br>";echo "<br>";
	}
$plain_text = "Hellopasswordthisisraisali";	 // plain_text for encryption
$length_plain_text=strlen($plain_text);
// TOTAL GROUP
$i=0;
	echo " plain_text is - ".$plain_text."<br>";
	echo " plain_text_size is - ".$length_plain_text."<br>";   //length of plain text
	echo "<br><br><br>";
   $total=ceil($length_plain_text/8);   //group of plain_texts
	$sub_plain_text = array(); 
$k=0;
//SUB_PLAIN_TEXT_ARRAY
for ($i = 0;$i<$total; $i++)
{$a=substr($plain_text,$k,8);
 $k=$k+8;
 array_push($sub_plain_text, $a);
}
  $rest=(8-strlen($sub_plain_text[$total-1]));
	
	
$i=0;$j=0;$m=0;

	
// CONVERT SUBGROUP_TEXT TO BINARY
	
$binary = array(); 	
$names = array();
for ($i = 0;$i<$total; $i++)
	{  $name;	
       $strlen = strlen($sub_plain_text[$i] );

		
		
	$bitbinary = array();
			
for( $j = 0; $j < $strlen; $j++ )
{
    $char = substr( $sub_plain_text[$i], $j, 1 );
	
	$hexv=ord($char);
	
	
// 8 BIT BINATRY FOR EACH LETTER	
	for ( $k= 7; $k >= 0; $k--)
    {
        $x= ($hexv&(1 << $k))? 1 :0;
	array_push($bitbinary, $x);
    }
 }
	$name=implode("",$bitbinary);
		array_push($names, $name); 
		array_push($binary, $names[$m]);
		$m=$m+1;
		unset($bitbinary);	
		$name=NULL;

}
// FOR LAST GROUP	
	$y = array();
	for ( $k= 1; $k <= (8*$rest); $k++)
    {
        $x=0;
	array_push($y, $x);
    
      
    }
	
	$name=implode("",$y);
	$binary[$total-1].=$name;
	
// DES_START
	
$key_64=array();
$key_56=array(); 
$key_48=array();	

$C=array();
$D=array();
$Bb = array();
$Bc=array();
$cipher_text=array();

		
//64 BIT KEY	
 for ($i=0;$i<64;$i++)
{ array_push($key_64,rand(0,1)); }

//56 BIT KEY				
for( $i = 0; $i <56; $i++ )
{
  
	 array_push($key_56, $key_64[$permute_table_56[$i]]);
	
}	
//48 BIT KEY
for( $i = 0; $i <48; $i++ )
{
  
	 array_push($key_48, $key_56[$permute_table_48[$i]]);
	
}//DES FOR SUB_PLAIN_TEXTS
for($m=0;$m<$total;$m++)	
{	echo "<br><br><br><br><br>sub_plain_text- ".$m."  and   corresponding binary_value  <br>";
	echo $sub_plain_text[$m]."<br>".$binary[$m]."<br><br>encryption rounds<br><br>";
	
 $A=array();
 $B=array();	
// BINARY_64_PERMUTE	
	
for( $j = 0; $j <64; $j++ )
{
   $binary[$m][$j]=$binary[$m][$permute_table_64[$j]]; }	
	
for( $j = 0; $j <64; $j++ )
{
   
	array_push($A,$binary[$m][$j]);$j++;
	array_push($B,$binary[$m][$j]);

	
}
	
	
for( $n =1; $n <=16; $n++)
 {
	 echo "<br>ROUND ".$n."<br>";
	 for ($i = 0;$i<32; $i++)
	   { echo $A[$i];}	
	for ($i = 0;$i<32; $i++)
	{ echo $B[$i];}
	 
	

$C=array();
$D=array();
$Bb = array();
$Bc=array();
	
	
for( $j = 0; $j <32; $j++ )
{
   
	array_push($C,$B[$j]);

}	
	
if(($n==1)||($n==2)||($n==9)||($n==16))
{
	$x=$key_56[0];	
for( $j = 0; $j <55; $j++ )
{      $key_56[$j]=$key_56[$j+1]; }
	$key_56[55]=$key_56[0];
  
}
	else
{
	
	$x=$key_56[0];
	$y=$key_56[1];
for( $j = 0; $j <54; $j++ )
{      $key_56[$j]=$key_56[$j+1];
}
	$key_56[54]=$key_56[0];
	$key_56[55]=$key_56[0];
  
}	
			
//KEY 48  BIT	
	 
for( $j = 0; $j <48; $j++ )
{
$key_48[$j]=$key_56[$permute_table_48[$j]];
}	

expension($B,$Bb);
for( $j = 0; $j <48; $j++ )
{
     $Bb[$j]=($key_48[$j] ^ $Bb[$j]);
	// echo $Bb[$j]."<br>";
	
}
	
compresion($Bb,$Bc,$s_box_substitution);	
for( $j = 0; $j <32; $j++ )
{ 
	 $Bc[$j]= $Bc[$permute_table_32[$j]];
}
		
for( $j = 0; $j <32; $j++ )
{
	$t=($key_48[$j] ^ $Bb[$j]);
   
    	// echo $t."<br>";
	array_push($D, $t);
	
}
	for( $j = 0; $j <32; $j++ )
{ $A[$j]=$C[$j];}

	for( $j = 0; $j <32; $j++ )
 { $B[$j]=$D[$j]; }
	
unset($C);	
unset($D);	
unset($Bb);	
unset($Bc);	
 }
		for($i=0;$i<32;$i++)
		 array_push($cipher_text, $A[$i]);
		for($i=0;$i<32;$i++)
		 array_push($cipher_text, $B[$i]);
unset($A);	
unset($B);	
}	
	
	// $name=implode("",$cipher_text);
echo "<br><br><br>";
$total=sizeof($cipher_text);
echo "<br>".sizeof($cipher_text)."<br>";
	for($i=0;$i<256;$i++)
		echo $cipher_text[$i]." ";
$cipher_db=array();

$i=0;$x=0;
while($i<$total)
{
	  $data=array();
	for($j=$i;$j<($i+8);$j++)
	{
       array_push($data,$cipher_text[$j]);

	}
	$name=implode("",$data);
	

	array_push($cipher_db, bindec($name));
	// echo bindec($name)."" ;
	$name=NULL;unset($data);

	$i=$i+8;


}
	$total=sizeof($cipher_db);		
echo "<br>".sizeof($cipher_db)."<br>";

  for($i=0;$i<$total;$i++)
  	echo $cipher_db[$i]." ";

	
function expension(&$B,&$Bb)
{
	$i=0;$j=0;$k=0;$y=0;
	$x=$B[31];
    while($k<8)
	{  
		array_push($Bb,$x);
		array_push($Bb,$B[$i]);$i++;
		array_push($Bb,$B[$i]);$i++;
		array_push($Bb,$B[$i]);$i++;
		array_push($Bb,$B[$i]);$i++;
	    
		if($i!=32)
		array_push($Bb,$B[$i]);
		else
			array_push($Bb,$B[0]);
	   // $x=$j-1; 
		$x=$B[((4*$k)+3)];
		$k++;
	}
}
function compresion(&$Bb,&$Bc,&$s_box_substitution)
{

	$j=0;
//	echo "<br>";
$x=0;
while($j<48)
	{
	    $a=$Bb[$j];$j++;
		$b=$Bb[$j];$j++;
		$c=$Bb[$j];$j++;
		$d=$Bb[$j];$j++;
		$e=$Bb[$j];$j++;
		$f=$Bb[$j];$j++;
		$var1=$a.$f;
		$var2=$b.$c.$d.$d;
		$var1=bindec($var1);  //for s_matrix index _row
     	$var2=bindec($var2);   //for s_matrix index_colomn
for ($i = 0;$i<4; $i++)
{
		$a=$s_box_substitution[$x][$var1][$var2][$i];
	  array_push($Bc,$a);
}
$x++;

	}
}
	
?>
	</body>
	</html>

